using System.Reflection;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.IO;
using Newtonsoft.Json.Linq;
using TVSI.XTRADE.BO.API.Common;

namespace TVSI.Trading.API.Middlewares;

public class LoggingMiddleware
{
    private readonly ILogger _logger;
    private readonly RequestDelegate _next;

    public LoggingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
    {
        _next = next;
        _logger = loggerFactory.CreateLogger<LoggingMiddleware>();
    }

    public async Task Invoke(HttpContext context)
    {
        var processId = Guid.NewGuid().ToString().Replace("-", "");
        await LogRequest(context, processId);
        await LogResponse(context, processId);
    }

    private async Task LogRequest(HttpContext context, string processId)
    {
        try
        {
            context.Request.EnableBuffering();
            var text = "File import đã được tiếp nhận.";
            var value = context.Request.Path.Value?.Split("/");
            var exceptionCode = value?.LastOrDefault()?.Length <= 6
                ? value?.LastOrDefault()
                : value?.LastOrDefault()?[..6]?.ToUpper();

            if (exceptionCode != "IMPORT")
            {
                await using var requestStream = new MemoryStream();
                await context.Request.Body.CopyToAsync(requestStream);
                text = ReadStream(requestStream);
            }

            _logger.LogInformation($"REQUEST Id:{processId}\n" +
                                   $"{context.Request.Method} {context.Request.Scheme}://{context.Request.Host.Value}{context.Request.Path.Value}{context.Request.QueryString} \n" +
                                   $"Body: {text}");
            context.Request.Body.Position = 0;
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
        }
    }

    private string ReadStream(Stream stream)
    {
        try
        {
            const int bufferLength = 4096;
            stream.Seek(0, SeekOrigin.Begin);
            using var textWriter = new StringWriter();
            using var reader = new StreamReader(stream);
            var buffer = new char[bufferLength];
            int readCount;
            do
            {
                readCount = reader.ReadBlock(buffer, 0, bufferLength);
                textWriter.Write(buffer, 0, readCount);
            } while (readCount > 0);

            var body = textWriter.ToString();
            return !body.IsJsonString() ? body : body.WriteLogNonePassword().ToString();
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return string.Empty;
        }
    }

    private async Task LogResponse(HttpContext context, string processId)
    {
        try
        {
            var originalBodyStream = context.Response.Body;

            await using var responseBody = new MemoryStream();
            context.Response.Body = responseBody;

            await _next(context);

            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var text = await new StreamReader(context.Response.Body).ReadToEndAsync();
            context.Response.Body.Seek(0, SeekOrigin.Begin);

            var value = context.Request.Path.Value?.Split("/");
            var exportCode = value?.LastOrDefault()?.Length <= 6
                ? value?.LastOrDefault()
                : value?.LastOrDefault()?[..6]?.ToUpper();
            text = exportCode switch
            {
                "EXPORT" => "Export file đã hoàn thành",
                _ => text
            };

            var downloadCode = value?.LastOrDefault()?.Length <= 8
                ? value?.LastOrDefault()
                : value?.LastOrDefault()?[..8]?.ToUpper();
            text = downloadCode switch
            {
                "DOWNLOAD" => "Download file template đã hoàn thành",
                _ => text
            };


            _logger.LogInformation($"RESPONSE Id:{processId}\n" +
                                   $"StatusCode: {context.Response.StatusCode}\n" +
                                   $"Body: {text}");

            await responseBody.CopyToAsync(originalBodyStream);
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
        }
    }
}