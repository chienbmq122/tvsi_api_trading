using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace TVSI.Trading.API.Filters.SwaggerFilter;

/// <summary>
/// 
/// </summary>
public class SwaggerOptions : IConfigureOptions<SwaggerGenOptions>
{
    private readonly IApiVersionDescriptionProvider _provider;
    /// <summary>
    /// 
    /// </summary>
    /// <param name="provider"></param>
    public SwaggerOptions(IApiVersionDescriptionProvider provider) => _provider = provider;


    /// <summary>
    /// 
    /// </summary>
    /// <param name="options"></param>
    public void Configure(SwaggerGenOptions options)
    {
        foreach (var description in _provider.ApiVersionDescriptions)
        {
            options.SwaggerDoc(description.GroupName, CreateApiVersion(description));
        }
    }
    private static OpenApiInfo CreateApiVersion(ApiVersionDescription description)
    {
        var info = new OpenApiInfo
        {
            Title = $"{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")} - TVSI TRADING API",
            Description = $"API for TRADING",
            Version = description.ApiVersion.ToString(),
            Contact = new OpenApiContact
            {
                Name = "TVSI Securities",
                Url = new Uri("https://www.tvsi.com.vn"),
            }
        };

        if (description.IsDeprecated)
        {
            info.Description += " - This API version has been deprecated.";
        }

        return info;
    }
    
}