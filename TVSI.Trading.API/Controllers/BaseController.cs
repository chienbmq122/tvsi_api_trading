using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Wangkanai.Detection.Services;

namespace TVSI.Trading.API.Controllers;

[Produces("application/json")]
[ApiController]
[Route("api/[controller]")]

/*[Authorize]*/
public class BaseController<T> : ControllerBase where T : class
{
    protected readonly IConfiguration _config;
    protected readonly IDetectionService _detection;
    protected readonly ILogger<T> _logger;
    
    /// <summary>
    /// Controller Constructor
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="config"></param>
    /// <param name="detection"></param>
    public BaseController(ILogger<T> logger, IConfiguration config, IDetectionService detection)
    {
        _logger = logger;
        _config = config;
        _detection = detection;
    }
    /// <summary>
    /// Xu ly log exception
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="message"></param>
    protected void LogException(string userName, string message)
    {
        Log.ForContext("Name", "exception").Error(message);
        if (!string.IsNullOrEmpty(userName))
        {
            Log.ForContext("Name", userName).Error(message);
        }
    }

}
