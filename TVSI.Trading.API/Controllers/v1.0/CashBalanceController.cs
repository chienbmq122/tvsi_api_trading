﻿using System.Reflection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TVSI.Trading.API.Models.Model;
using TVSI.Trading.API.Models.Model.Request.CashBalance;
using TVSI.Trading.API.Service.Interfaces;
using Wangkanai.Detection.Services;

namespace TVSI.Trading.API.Controllers.v1._0;


/// <summary>
/// Trading Module API
/// </summary>
[ApiVersion("1.0")]
public class CashBalanceController :  BaseController<CashBalanceController>
{

    private readonly ICashBalanceService _balanceService;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="config"></param>
    /// <param name="detection"></param>
    /// <param name="balanceService"></param>
    public CashBalanceController(ILogger<CashBalanceController> logger, IConfiguration config, IDetectionService detection,ICashBalanceService balanceService) : base(logger, config, detection)
    {
        _balanceService = balanceService;
    }
    
    /// <summary>
    ///  Lấy số dư tiền có thể rút
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /api/CashBalance/GetBalanceWidthrawal
    ///     {
    ///         "UserName": "1001-001",
    ///         "CustCode": "000198"
    ///     }
    /// </remarks>
    [AllowAnonymous]
    [HttpPost("GetBalanceWidthrawal")]
    public async Task<IActionResult> GetBalanceWidthrawal(CashBalanceRequest model)
    {
        try
        {
            var response = await _balanceService.GetBalanceWithdrawalCustomer(model);
            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return StatusCode(StatusCodes.Status500InternalServerError, "");

        }
    }
}