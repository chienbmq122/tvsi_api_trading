﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using TVSI.Trading.API.Models.Model;
using TVSI.Trading.API.Models.Model.Request.Common;
using TVSI.Trading.API.Service.Interfaces;
using Wangkanai.Detection.Services;

namespace TVSI.Trading.API.Controllers.v1._0
{
    /// <summary>
    /// Common Module API
    /// </summary>
    public class CommonController : BaseController<CommonController>
    {
        private readonly ICommonService _commonService;
        
        
        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="config"></param>
        /// <param name="detection"></param>
        /// <param name="commonService"></param>
        public CommonController(ILogger<CommonController> logger, IConfiguration config,
        IDetectionService detection, ICommonService commonService) : base(logger, config, detection)
        {
            _commonService = commonService;
        }

        /// <summary>
        ///  lấy danh sách tổng hợp giao dịch
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /api/Common/GetStockSymbol
        ///     {
        ///         "UserId": "1001-001"
        ///     }
        /// </remarks>
        [AllowAnonymous]
        [HttpPost("GetStockSymbol")]
        public async Task<IActionResult> GetStockSymbol(StockSymbolRequest model)
        {
            try
            {
                var response = await _commonService.GetStockSymbol(model);
                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, "");

            }
        }  
        
        /// <summary>
        ///  lấy trạng thái giao dịch trong ngày
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /api/Common/GetOrderStatus
        ///     {
        ///         "UserName": "1001-001"
        ///     }
        /// </remarks>
        [AllowAnonymous]
        [HttpPost("GetOrderStatus")]
        public async Task<IActionResult> GetOrderStatus(BaseRequest model)
        {
            try
            {
                var response = await _commonService.GetOrderStatus(model);
                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, "");

            }
        }
        
        
        
        /// <summary>
        /// API  Lấy danh sách phân quyền
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns> API lấy danh sách trạng thái hoạt động hoặc đóng của danh sách khách hàng</returns>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Common/GetCustomerStatus
        ///      {
        ///         "UserName": "6245-001",
        ///         "UserRole": "string"
        ///         
        ///      }
        /// </remarks>
        [HttpPost("GetAllRightByFun")]
        public async Task<IActionResult> GetAllRightByFun(GetAllRightByFunctionRequest model, [FromQuery] string? src = "M",
            [FromQuery] string? lang = "VI")
        {
            try
            {
                var response = await _commonService.GetAllRightByFun(model.UserName,model.Function);
                return Ok(response);
            }
            catch (Exception ex)
            {
                LogException(model.UserName,
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, "");
            }
        }
    }
}
