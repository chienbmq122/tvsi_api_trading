using System.Reflection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TVSI.Trading.API.Models.Model;
using TVSI.Trading.API.Models.Model.Request.TradingDaily;
using TVSI.Trading.API.Service.Interfaces;
using Wangkanai.Detection.Services;

namespace TVSI.Trading.API.Controllers.v1._0;

/// <summary>
/// Trading Module API
/// </summary>
[ApiVersion("1.0")]
public class TradingController : BaseController<TradingController>
{
    private readonly ITradingDailyService _tradingDailyService;

    /// <summary>
    /// Controller Constructor
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="config"></param>
    /// <param name="detection"></param>
    /// <param name="tradingDailyService"></param>
    public TradingController(ILogger<TradingController> logger, IConfiguration config,
        IDetectionService detection, ITradingDailyService tradingDailyService) : base(logger, config, detection)
    {
        _tradingDailyService = tradingDailyService;
    }
    
    
    /// <summary>
    /// danh sách giao dịch trong ngày
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns> danh sách giao dịch trong ngày</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Trading/GetTradingDailyList
    ///      {
    ///        "UserName": "1001-001",
    ///        "CustCode": "",
    ///        "PageSize": 1,
    ///        "PageIndex": 10
    ///      }
    /// </remarks>
    [HttpPost("GetTradingDailyList")]
    public async Task<IActionResult> GetTradingDailyList(TradingDailyRequest model)
    {
        try
        {
            var response = await _tradingDailyService.GetTradingDailyList(model);
            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return StatusCode(StatusCodes.Status500InternalServerError, "");

        }
    }
    
    /// <summary>
    /// Chi tiết giao dịch trong ngày
    /// </summary>
    /// <param name="model"></param>
    /// <param name="src"></param>
    /// <param name="lang"></param>
    /// <returns> Chi tiết giao dịch trong ngày</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /Trading/GetTradingDailyDetail
    ///      {
    ///        "UserName": "1001-001",
    ///        "CustCode": "",
    ///        "StockCode": "",
    ///        "Side": "",
    ///        "OrderStatus": 0
    ///      }
    /// </remarks>
    [HttpPost("GetTradingDailyDetail")]
    public async Task<IActionResult> GetTradingDailyDetail(TradingDailyDetailRequest model)
    {
        try
        {
            var response = await _tradingDailyService.GetTradingDailyDetailInfo(model);
            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return StatusCode(StatusCodes.Status500InternalServerError, "");

        }
    }

    /// <summary>
    ///  lấy danh sách tổng hợp giao dịch
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /api/Trading/GetTradingTotal
    ///     {
    ///         "UserId": "1001-001"
    ///     }
    /// </remarks>
    [AllowAnonymous]
    [HttpPost("GetTradingTotal")]
    public async Task<IActionResult> GetTradingTotal(BaseRequest model)
    {
        try
        {
            var response = await _tradingDailyService.GetTradingTotal(model);
            return Ok(response);
        }
        catch (Exception ex)
        {
            LogException(model.UserName,
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return StatusCode(StatusCodes.Status500InternalServerError, "");

        }
    }
}