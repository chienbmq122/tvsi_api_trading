using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;
using TVSI.Trading.API.Filters.SwaggerFilter;
using TVSI.Trading.API.Service.Impls;
using TVSI.Trading.API.Service.Interfaces;

namespace TVSI.Trading.API.DI;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddServices(this IServiceCollection services)
    {
        #region Core Service
        services.AddScoped<IDapperHelper, DapperHelper>();
        services.AddTransient<IConfigureOptions<SwaggerGenOptions>, SwaggerOptions>();

        #endregion Core Service

        #region service
        
        services.AddScoped<ITradingDailyService, TradingDailyService>();
        services.AddScoped<ICommonService, CommonService>();
        services.AddScoped<ICashBalanceService, CashBalanceService>();

        #endregion  service

        return services;
    }

}