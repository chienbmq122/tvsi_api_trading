
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Prometheus;
using Serilog;
using TVSI.Trading.API.DI;
using TVSI.Trading.API.Filters.SwaggerFilter;
using TVSI.Trading.API.Middlewares;

var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;
var logging = builder.Logging;
var services = builder.Services;

#region AppSetting

configuration
    .AddJsonFile("appsettings.json", false, true)
    .AddJsonFile($"appsettings.{env}.json", true, true)
    .AddEnvironmentVariables();

#endregion AppSetting

#region Log4net

logging
    .SetMinimumLevel(LogLevel.Trace)
    .AddLog4Net("log4net.config");

#endregion Log4net

#region Detection

services.AddDetection();
services.AddDetectionCore().AddBrowser();
services.AddDetectionCore().AddPlatform();
services.AddDetectionCore().AddEngine();
services.AddDetectionCore().AddDevice();

#endregion Detection

#region ApiVersioning

services.AddApiVersioning(config =>
{
    config.DefaultApiVersion = new ApiVersion(1, 0);
    config.AssumeDefaultVersionWhenUnspecified = true;
    config.ReportApiVersions = true;
    config.ApiVersionReader = ApiVersionReader.Combine(
        new HeaderApiVersionReader("x-api-version"),
        new QueryStringApiVersionReader("v"));
});

services.AddVersionedApiExplorer(options =>
{
    options.GroupNameFormat = "'v'VVV";
    options.SubstituteApiVersionInUrl = true;
});

#endregion ApiVersioning
#region Authentication

services.AddAuthentication(option =>
{
    option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuer = true,
        ValidIssuer = configuration["Jwt:Issuer"],
        ValidateAudience = true,
        ValidAudience = configuration["Jwt:Audience"],
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Secret"]!)),
        RequireExpirationTime = false,
        ValidateLifetime = true,
        ClockSkew = TimeSpan.Zero
    };
});

#endregion Authentication

#region Swagger

services.AddServices();
services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

#endregion Swagger
services.AddEndpointsApiExplorer();
services.AddSwaggerGen(c =>
{
    c.OperationFilter<OperationFilter>();
    var xmlPath = Path.Combine(AppContext.BaseDirectory, "TVSI.Trading.API.xml");
    if (File.Exists(xmlPath))
        c.IncludeXmlComments(xmlPath);

    var xmlLibPath = Path.Combine(AppContext.BaseDirectory, "TVSI.Trading.API.Service.xml");
    if (File.Exists(xmlLibPath))
        c.IncludeXmlComments(xmlLibPath);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsProduction())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        var apiVersionDescriptions
            = app.Services.GetService<IApiVersionDescriptionProvider>()?.ApiVersionDescriptions;
        if (apiVersionDescriptions == null) return;
        foreach (var description in apiVersionDescriptions)
            c.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
                description.GroupName);
    });
   
}



app.UseHttpsRedirection();
app.UseRouting();
app.UseCustomMiddleware();
app.UseCors(x => x
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());

app.UseHttpMetrics();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapGet("/",
        async context => { await context.Response.WriteAsync("TVSI - Trading API"); });
});

app.MapControllers();
app.Run();
