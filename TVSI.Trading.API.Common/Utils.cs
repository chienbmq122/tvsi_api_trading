﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Net.Http.Headers;
using System.Net.Http.Json;
/*
using System.Data.Entity.Validation;
*/
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using DocumentFormat.OpenXml.Packaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static System.Net.Http.HttpMethod;

namespace TVSI.XTRADE.BO.API.Common;

public static class Utils
{
    public static string ErrorCodeFormat(this int errCode)
    {
        return !string.IsNullOrEmpty(CommonConstants.ErrPrefix)
            ? errCode != 1 ? $"{CommonConstants.ErrPrefix}-{errCode}" : errCode.ToString()
            : errCode.ToString();
    }

    public static string Utf8Convert(this string s)
    {
        var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
        var temp = s.Normalize(NormalizationForm.FormD);
        return regex.Replace(temp, string.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
    }

    public static string ReplaceWhitespace(this string input, string replacement = "")
    {
        var sWhitespace = new Regex(@"\s+");
        return sWhitespace.Replace(input, replacement);
    }

    public static string ReplaceSpecialCharacter(this string input, string replacement = "_")
    {
        var specialChar = new Regex(@"[~`!@#$%^&*()+={[}\]|':;<,>.?/-]");
        return specialChar.Replace(input, replacement);
    }

    public static string UpdateFileJson(string filePath, string[] pathKeys, string[] arrVal)
    {
        var jsonString = File.ReadAllText(filePath);
        var jObject = JsonConvert.DeserializeObject(jsonString) as JObject;
        for (var i = 0; i < pathKeys.Length; i++)
        {
            var jToken = jObject?.SelectToken(pathKeys[i]);
            jToken?.Replace(arrVal[i]);
        }

        var updatedJsonString = jObject?.ToString();
        return updatedJsonString ?? string.Empty;
    }

    public static bool CompareModel<T>(this T current, T orig)
    {
        var properties = TypeDescriptor.GetProperties(typeof(T));
        return !(from PropertyDescriptor property in properties
            let currentValue = property.GetValue(current)
            let grinValue = property.GetValue(orig)
            where currentValue.ToString() != grinValue.ToString()
            select currentValue).Any();
    }

    /*public static string GetMessageDbEntityValidationException(this DbEntityValidationException e)
    {
        return e.EntityValidationErrors.Aggregate(string.Empty, (current1, eve)
            => eve.ValidationErrors.Aggregate(current1, (current, ve)
                => current + "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + ", "));
    }*/

    #region Hasing data

    public static string GetSalt()
    {
        const int length = 32;
        var random = new RNGCryptoServiceProvider();
        var salt = new byte[length];
        random.GetNonZeroBytes(salt);
        return Convert.ToBase64String(salt);
    }

    public static string ComputeSha256Hash(string username, string salt, string pass)
    {
        var crypt = new SHA256Managed();
        var hash = new StringBuilder();
        var crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes($"{username} - {salt} - {pass}"));
        foreach (var theByte in crypto) hash.Append(theByte.ToString("x2"));
        return hash.ToString();
    }

    public static bool CheckLoginWithPassword(string username, string pass, string salt, string passHash)
    {
        var isValid = ComputeSha256Hash(username, salt, pass).Equals(passHash);
        return isValid;
    }

    public static string CreateMd5(this string input)
    {
        var md5 = new MD5CryptoServiceProvider();
        var bHash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
        var sbHash = new StringBuilder();
        foreach (var b in bHash) sbHash.Append($"{b:x2}");
        return sbHash.ToString();
    }

    public static string MD5EncodePassword(this string originalPassword)
    {
        MD5 md5 = new MD5CryptoServiceProvider();
        var originalBytes = Encoding.Default.GetBytes(originalPassword);
        var encodedBytes = md5.ComputeHash(originalBytes);
        return BitConverter.ToString(encodedBytes);
    }

    public static string EncryptionMd5(this string content)
    {
        var textBytes = Encoding.Default.GetBytes(content);
        var cryptHandler = new MD5CryptoServiceProvider();
        var hash = cryptHandler.ComputeHash(textBytes);
        return hash.Aggregate("", (current, a) => current + a.ToString("x2"));
    }

    public static string RandomNumber(this int length)
    {
        var seed = Guid.NewGuid().ToByteArray();
        var random = new Random(BitConverter.ToInt32(seed, 0));
        var randomNumber = "";
        for (var i = 0; i < length; i++) randomNumber += (char)random.Next(48, 58);
        return randomNumber;
    }

    public static string CreateCheckSum(this string input)
    {
        var result = CreateMd5(input);
        return result;
    }

    #endregion Hasing data

    #region Enums

    public static string ToEnumDescription(this Enum val, bool isUpper = false)
    {
        var attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString())
            ?.GetCustomAttributes(typeof(DescriptionAttribute), false)!;
        return attributes.Length > 0
            ? isUpper ? attributes[0].Description.ToUpper() : attributes[0].Description
            : string.Empty;
    }

    public static T? ToEnumVal<T>(this string description) where T : Enum
    {
        foreach (var field in typeof(T).GetFields())
            if (Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
            {
                if (attribute.Description == description)
                    return (T)field.GetValue(null)!;
            }
            else
            {
                if (field.Name == description)
                    return (T)field.GetValue(null)!;
            }

        return default;
    }

    #endregion Enums

    #region To new Object

    public static string[] ToArray(this string input, char delimiter)
    {
        return input.Split(delimiter);
    }

    public static string ToJsonObject(this DataTable dt)
    {
        return JsonConvert.SerializeObject(dt);
    }

    public static string ToJsonObject(this DataSet ds)
    {
        return JsonConvert.SerializeObject(ds);
    }

    public static List<Dictionary<string, object>> ToJsonList(this DataTable dt)
    {
        return (from DataRow dr in dt.Rows
            select dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => dr[col])).ToList();
    }

    public static ArrayList ToJsonList(this DataSet ds)
    {
        var root = new ArrayList();

        foreach (DataTable dt in ds.Tables)
        {
            var table = (from DataRow dr in dt.Rows
                select dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => dr[col])).ToList();
            root.Add(table);
        }

        return root;
    }

    public static List<T> ToList<T>(this DataTable dt)
    {
        return (from DataRow row in dt.Rows select GetItem<T>(row)).ToList();
    }

    public static DataTable ToDataTable<T>(this IList<T> data)
    {
        var props = TypeDescriptor.GetProperties(typeof(T));
        var table = new DataTable();
        for (var i = 0; i < props.Count; i++)
        {
            var prop = props[i];
            table.Columns.Add(prop.Name, prop.PropertyType);
        }

        var values = new object?[props.Count];
        foreach (var item in data)
        {
            for (var i = 0; i < values.Length; i++) values[i] = props[i].GetValue(item);
            table.Rows.Add(values);
        }

        return table;
    }

    public static DataSet? ToDataSet(this string jsonString)
    {
        return JsonConvert.DeserializeObject<DataSet>(jsonString);
    }

    public static List<Dictionary<string, object?>?>? ToDictionary(this string jsonString)
    {
        var dict = new List<Dictionary<string, object>?>();
        if (!jsonString.IsJsonString()) return null;

        if (!jsonString.IsJsonArray())
        {
            dict.Add(jsonString.JObjectToDictionary());
            return dict;
        }

        var arrJson = JArray.Parse(jsonString);
        dict.AddRange(arrJson.Select(item => item.ToString().JObjectToDictionary()));

        return dict;
    }

    public static Dictionary<string, object>? JObjectToDictionary(this string jsonString)
    {
        if (!jsonString.IsJsonString()) return null;

        var obj = JObject.Parse(jsonString);
        var dict = new Dictionary<string, object>();

        foreach (var (name, value) in obj)
        {
            switch (value)
            {
                case JArray:
                    dict.Add(name, value.ToArray());
                    break;
                case JValue:
                    dict.Add(name, value.ToString());
                    break;
                case JObject:
                {
                    var jsonChildDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(value.ToString());
                    var childDict = jsonChildDict?.ToDictionary(x => x.Key, x => x.Value);

                    if (childDict != null) dict.Add(name, childDict);
                    break;
                }
                default:
                    throw new NotSupportedException("Invalid JSON token type.");
            }
        }

        return dict;
    }

    public static T GetItem<T>(DataRow dr)
    {
        try
        {
            var temp = typeof(T);
            var obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            foreach (var pro in temp.GetProperties())
                if (pro.Name == column.ColumnName)
                    pro.SetValue(obj, dr[column.ColumnName] == DBNull.Value ? null : dr[column.ColumnName], null);
            return obj;
        }
        catch (Exception ex)
        {
            return default;
        }
    }

    #endregion To new Object

    #region Validate

    public static bool IsBase64String(this string base64)
    {
        base64 = base64.Trim();
        return base64.Length % 4 == 0 && Regex.IsMatch(base64, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
    }

    public static bool IsJsonString(this string json)
    {
        if (string.IsNullOrEmpty(json)) return false;

        try
        {
            JToken.Parse(json);
        }
        catch (Exception)
        {
            return false;
        }

        return true;
    }

    public static bool IsJsonArray(this string json)
    {
        if (string.IsNullOrEmpty(json)) return false;
        return json.StartsWith("[") && json.EndsWith("]");
    }

    public static bool IsEmailAddress(this string emailAddress)
    {
        try
        {
            var m = new MailAddress(emailAddress);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public static bool IsVietnamesePhoneNumber(this string phoneNumber)
    {
        return Regex.IsMatch(phoneNumber, @"^[\+84|84|0]+([0-9]{9})\b$", RegexOptions.None);
    }

    #endregion Validate

    #region Json checked

    public static bool CompareJsonElements(this string json1, string json2)
    {
        var element1 = GetAllElements(json1).OrderBy(x => x).ToList();
        var element2 = GetAllElements(json2).OrderBy(x => x).ToList();
        return element1.SequenceEqual(element2);
    }

    public static IEnumerable<string> GetAllElements(string json)
    {
        var regex = new Regex(@"\[\d*\].", RegexOptions.Compiled);
        return JObject.Parse(json).DescendantsAndSelf()
            .OfType<JProperty>()
            .Where(jp => jp.Value is JValue)
            .Select(jp => regex.Replace(jp.Path, "."))
            .Distinct();
    }

    public static string RemoveJsonProperty(this string json, string property)
    {
        if (json.StartsWith("[") && json.EndsWith("]"))
        {
            var jTemp = JArray.Parse(json);

            jTemp.Descendants()
                .OfType<JProperty>()
                .Where(attr => attr.Name == property)
                .ToList()
                .ForEach(attr => attr.Remove());

            return jTemp.ToString();
        }
        else
        {
            var jTemp = JObject.Parse(json);

            jTemp.Descendants()
                .OfType<JProperty>()
                .Where(attr => attr.Name == property)
                .ToList()
                .ForEach(attr => attr.Remove());

            return jTemp.ToString();
        }
    }

    //Check exit json property from root key
    public static bool HasJsonProperty(this JObject json, string property)
    {
        return json.ContainsKey(property);
    }

    //Check exit json property from nested key
    public static bool ExistsJsonNestedProperty(this JObject json, string property)
    {
        return json.ContainsKey(property);
    }

    //Check exit json property from tree key
    public static bool ExistsTreeJsonProperty(this JObject json, string parentProperty, string childrenProperty)
    {
        if (json.ContainsKey(parentProperty))
        {
        }

        return false;
    }

    public static JObject WriteLogNonePassword(this string json)
    {
        if (!string.IsNullOrEmpty(json)) return JObject.Parse("{}");
        const string passwordChar = CommonConstants.PasswordChar;
        var jObject = JObject.Parse(json);
        if (jObject.HasJsonProperty("password")) jObject["password"] = passwordChar;
        if (jObject.HasJsonProperty("pass")) jObject["pass"] = passwordChar;
        if (jObject.HasJsonProperty("pwd")) jObject["pwd"] = passwordChar;
        if (jObject.HasJsonProperty("Password")) jObject["Password"] = passwordChar;
        if (jObject.HasJsonProperty("Pass")) jObject["Pass"] = passwordChar;
        if (jObject.HasJsonProperty("Pwd")) jObject["Pwd"] = passwordChar;

        return jObject;
    }

    #endregion Json checked

    #region File I/O

    public static string ReadFile(this string filePath)
    {
        FileStream objFileStream = null;
        try
        {
            objFileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var objReader = new StreamReader(objFileStream);
            var s = objReader.ReadToEnd();
            return s;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
        finally
        {
            objFileStream?.Close();
        }
    }

    public static void WriteFile(this string strContent, string filePath)
    {
        try
        {
            StreamWriter log;
            if (File.Exists(filePath)) File.Delete(filePath);
            log = new StreamWriter(filePath);
            // Write to the file:
            log.Write(strContent);
            log.WriteLine();
            log.Close();
        }
        catch (Exception ex)
        {
            // ignored
        }
    }

    public static string ReadFileUsingOpenXml(this string filePath)
    {
        using var doc = WordprocessingDocument.Open(filePath, false);
        return doc.MainDocumentPart?.Document.Body != null
            ? doc.MainDocumentPart.Document.Body.InnerText
            : string.Empty;
    }

    public static Dictionary<string, object> ReadContentFileJson(this string fileName) //fileName = internalIP.json
    {
        var jsonFile = $"{fileName}";
        var json = File.ReadAllText(
            $"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}{@"/"}{jsonFile}");
        var obj = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        return obj;
    }

    #endregion File I/O

    #region Bind data to Content

    public static string BindDataToContent(this JObject xData, string strContent)
    {
        try
        {
            var destList = new List<string>();
            var newContent = strContent.Replace("[", "<param>").Replace("]", "</param>");
            foreach (Match match in Regex.Matches(newContent, "<param>(.*?)</param>"))
                destList.Add(match.Groups[1].Value);

            if (xData.ContainsKey("Table")) xData = (JObject)xData["Table"]?[0];

            foreach (var item in destList)
            {
                var value = xData.GetValueJObjectByKey(item, item.Contains("."), "VN");
                if (item.Contains(".CKB."))
                {
                    strContent = strContent.Replace($"[{item}]", value);
                }
                else
                {
                    value = item.Equals(value) ? $"[{value}]" : HttpUtility.HtmlEncode(value);
                    strContent = strContent.Replace($"[{item}]", value);
                }
            }

            return strContent;
        }
        catch (Exception ex)
        {
            return strContent;
        }
    }

    public static string GetValueJObjectByKey(JObject xData, string key)
    {
        try
        {
            var x = xData[key];
            return x == null ? key : x.ToString();
        }
        catch (Exception)
        {
            return key;
        }
    }

    /// <summary>
    ///     Lấy giá tri trong data trả về theo key
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="key"></param>
    /// <param name="isDot">true: trường hợp dữ liệu kiểu Date,Num,DISPLAY v.v.v</param>
    /// <returns></returns>
    public static string GetValueJObjectByKey(this JObject dt, string key, bool isDot, string lang)
    {
        if (isDot == false) return GetValueJObjectByKey(dt, key);

        var keyLocal = GetValueByLocation(key, 1, '.'); //lay C_KEY
        switch (keyLocal)
        {
            case "TOLOWER":
            {
                return GetValueJObjectByKey(dt, key.Replace(".TOLOWER", "")).ToLower();
            }
            case "TOLOWER1":
            {
                //Viết hoa chữ đầu tiên
                var value = GetValueJObjectByKey(dt, key.Replace(".TOLOWER1", "")).ToLower();
                if (!string.IsNullOrEmpty(value))
                    value = char.ToUpper(value[0]) + value.Substring(1);
                return value;
            }
            case "TOUPPER":
            {
                return GetValueJObjectByKey(dt, key.Replace(".TOUPPER", "")).ToUpper();
            }
            case "FLOAT_TEXT":
            {
                var value = GetValueJObjectByKey(dt, key.Replace(".FLOAT_TEXT", ""));
                if (key.Contains(value) == false) //tra về value
                    return NumericFloatToString(value);
                break;
            }
            case "FLOAT":
            {
                var numberFormat = GetValueByLocation(key, 2, '.');
                var value = GetValueJObjectByKey(dt,
                    key.Replace(".FLOAT" + (string.IsNullOrEmpty(numberFormat) ? "" : "." + numberFormat), ""));
                double.TryParse(value, out var number);

                if (string.IsNullOrEmpty(numberFormat) || number.ToString().Contains(".") == false)
                    return number.ToString().Contains(".")
                        ? string.Format(number > 999 ? "{0:0,0.00}" : "{0:0.00}", number)
                        : string.Format(number > 999 ? "{0:0,0}" : "{0:0}", number);

                //form bao nhiêu so sau hang thập phan
                return numberFormat switch
                {
                    "1" => string.Format(number > 999 ? "{0:0,0.0}" : "{0:0.0}", number),
                    "2" => string.Format(number > 999 ? "{0:0,0.00}" : "{0:0.00}", number),
                    "3" => string.Format(number > 999 ? "{0:0,0.000}" : "{0:0.000}", number),
                    "4" => string.Format(number > 999 ? "{0:0,0.0000}" : "{0:0.0000}", number),
                    "5" => string.Format(number > 999 ? "{0:0,0.00000}" : "{0:0.00000}", number),
                    "6" => string.Format(number > 999 ? "{0:0,0.000000}" : "{0:0.000000}", number),
                    "7" => string.Format(number > 999 ? "{0:0,0.0000000}" : "{0:0.0000000}", number),
                    _ => string.Format(number > 999 ? "{0:0,0}" : "{0:0}", number)
                };
            }
            case "NUM_TEXT":
            {
                var value = GetValueJObjectByKey(dt, key.Replace(".NUM_TEXT", ""));
                if (key.Contains(value) == false) //tra về value
                    return NumericIntToString(value);
                break;
            }
            case "NUM":
            {
                var value = GetValueJObjectByKey(dt, key.Replace(".NUM", ""));
                double.TryParse(value, out var number);
                return number.ToString().Equals(".")
                    ? string.Format(number > 999 ? "{0:0,0.00}" : "{0:0.00}", number)
                    : string.Format(number > 999 ? "{0:0,0}" : "{0:0}", number);
            }
            case "CKB":
            {
                keyLocal = GetValueByLocation(key, 0, '.'); //lay C_KEY
                var value = GetValueJObjectByKey(dt, keyLocal); //lay value theo C_KEY

                if (value == keyLocal) //không co trong db
                    return "<w:sym w:font=\"Wingdings\" w:char=\"F06F\"/>";

                var strKey = GetValueByLocation(key, 2, '.');
                var lst = new List<string> { strKey };
                if (strKey.Contains("|")) lst = strKey.Split('|').ToList();

                return lst.Contains(value)
                    ? "<w:sym w:font=\"Wingdings\" w:char=\"F0FE\"/>"
                    : "<w:sym w:font=\"Wingdings\" w:char=\"F06F\"/>";
            }
            case "DISPLAY":
            {
                keyLocal = GetValueByLocation(key, 0, '.'); //lay C_KEY
                var value = GetValueJObjectByKey(dt, keyLocal); //lay value theo C_KEY

                if (value == keyLocal || value != GetValueByLocation(key, 2, '.')) //không co trong db
                    return "display:none";

                return "display:block";
            }
            case "D":
            {
                var value = GetValueJObjectByKey(dt, key.Replace(".D", ""));
                return GetValueByLocation(value, 0, '/');
            }
            case "M":
            {
                var value = GetValueJObjectByKey(dt, key.Replace(".M", ""));
                return GetValueByLocation(value, 1, '/');
            }
            case "Y":
            {
                var value = GetValueJObjectByKey(dt, key.Replace(".Y", ""));
                return GetValueByLocation(value, 2, '/');
            }
            case "READ":
            {
                //Doc so thanh chu vi du: 1 thang, 2 tuần => mot thang, hai tuan v.v.v
                var value = GetValueJObjectByKey(dt, GetValueByLocation(key, 0, '.'));
                if (string.IsNullOrEmpty(value))
                    return value;

                var strNumber = GetValueByLocation(key, 2, '.');
                int.TryParse(strNumber, out var number);
                if (number <= 0) return "";

                strNumber = GetValueByLocation(value, number - 1, ' ');
                int.TryParse(strNumber, out number);
                if (string.IsNullOrEmpty(strNumber) == false && number > 0)
                    return value.Replace(strNumber, NumericIntToString(strNumber));
                return "";
            }
            case "NUMDISPLAYBYCONDITION":
            {
                var fieldGetValue = GetValueByLocation(key, 2, '.');
                var valueOfField = GetValueByLocation(key, 3, '.');
                var valueOfFiledCheck = GetValueJObjectByKey(dt, fieldGetValue);

                if (valueOfField != valueOfFiledCheck) return "";

                var value = GetValueJObjectByKey(dt, GetValueByLocation(key, 0, '.'));
                double.TryParse(value, out var number);
                if (number.ToString().Equals("."))
                    return string.Format(number > 999 ? "{0:0,0.00}" : "{0:0.00}", number) + " VNĐ";

                return string.Format(number > 999 ? "{0:0,0}" : "{0:0}", number) + " VNĐ";
            }
            default:
                return key;
        }

        return key;
    }

    /// <summary>
    ///     lấy vị trí cắt chuỗi trong hàm split: ví lấy ngày tháng: dd/MM/yyyy
    /// </summary>
    /// <param name="plainText"></param>
    /// <param name="location">bắt đầu từ 0</param>
    /// <param name="delimiter"></param>
    /// <returns></returns>
    public static string GetValueByLocation(string plainText, int location, char delimiter)
    {
        var array = plainText.Split(delimiter);
        return array.Length > location ? array[location] : "";
    }

    public static string NumericFloatToString(string pStrNumeric)
    {
        if (!pStrNumeric.Contains(".") && !pStrNumeric.Contains(","))
            return NumericIntToString(pStrNumeric);

        var countNumber = pStrNumeric.Count(ch => ch == '.');
        var countNumber2 = pStrNumeric.Count(ch => ch == ',');
        if (countNumber + countNumber2 != 1)
            return NumericIntToString(pStrNumeric);

        var lstSplit = countNumber == 1 ? pStrNumeric.Split('.').ToList() : pStrNumeric.Split(',').ToList();
        return NumericIntToString(lstSplit[0]) + DecimalToString_AfterCommas(lstSplit[1]).ToLower();
    }

    /// <summary>
    ///     Hàm đọc số thành chữ
    /// </summary>
    /// <param name="pStrNumeric"></param>
    /// <returns></returns>
    public static string NumericIntToString(string pStrNumeric)
    {
        var count = 0;
        // Loại bỏ dấu phân cách nhóm
        var strNumber = pStrNumeric.Replace(",", "");
        strNumber = strNumber.Replace(".", "");

        // Loại bỏ số 0 đứng đầu
        var arr = strNumber.ToCharArray();
        int i;
        for (i = 0; i < arr.Length; i++)
            if (arr[i] == 0)
                count++;
            else
                break;
        strNumber = strNumber.Substring(count);

        // Doc chuỗi so
        var len = strNumber.Length;
        // Dem bo so 3
        var len3 = len / 3;
        var mod3 = len % 3;

        // Neu doi dai chuỗi la 0 thi gan la so 0
        if (len == 0)
        {
            strNumber = "0";
            mod3 = 1;
        }

        // Doc bo so dau tiên
        var strRead3 = Len3ToString(strNumber.Substring(0, mod3));
        var strReadNumber = strRead3;
        for (i = 0; i < len3; i++)
        {
            if (3 * (len3 - i) % 9 == 0 && strReadNumber.Length > 0) strReadNumber += " tỷ";
            if (strRead3.Length > 0)
            {
                if (3 * (len3 - i) % 9 == 6) strReadNumber += " triệu";
                if (3 * (len3 - i) % 9 == 3) strReadNumber += " nghìn";
            }

            // Đọc chuỗi 3 kí tự
            strRead3 = Len3ToString(strNumber.Substring(mod3 + i * 3, 3));
            // Luu chuỗi còn lại
            // strNumber = strNumber.Substring(mod3 + i * 3);
            // Gán vào chuỗi kết quả đọc
            strReadNumber += strRead3;
        }

        return strReadNumber.Substring(1, 1).ToUpper() + strReadNumber.Substring(2);
    }

    /// <summary>
    ///     Hàm đọc chuỗi ký tự số có độ dài là 3
    /// </summary>
    /// <param name="pStrInt"></param>
    /// <returns></returns>
    public static string Len3ToString(string pStrInt)
    {
        var strNumber = "";
        var len = pStrInt.Length;
        if (pStrInt == "000" || pStrInt == "") return strNumber;

        var arr = pStrInt.ToCharArray();
        switch (arr[0])
        {
            case '0':
                if (len == 3 || len == 1) strNumber += " không";
                break;

            case '1':
                if (len == 2)
                    strNumber += " mười";
                else
                    strNumber += " một";
                break;

            case '2':
                strNumber += " hai";
                break;

            case '3':
                strNumber += " ba";
                break;

            case '4':
                strNumber += " bốn";
                break;

            case '5':
                strNumber += " năm";
                break;

            case '6':
                strNumber += " sáu";
                break;

            case '7':
                strNumber += " bảy";
                break;

            case '8':
                strNumber += " tám";
                break;

            case '9':
                strNumber += " chín";
                break;
        }

        if (len == 3)
            // hàng Trăm
            strNumber += " trăm";
        if (len == 2)
            if (arr[0] != '0' && arr[0] != '1')
                strNumber += " mươi";
        // Hàng Chục
        if (len >= 2)
        {
            switch (arr[1])
            {
                case '1':
                    if (len == 3) strNumber += " mười";
                    if (len == 2 && arr[0] != '1') strNumber += " mốt";
                    if (len == 2 && arr[0] == '1') strNumber += " một";
                    break;

                case '2':
                    strNumber += " hai";
                    break;

                case '3':
                    strNumber += " ba";
                    break;

                case '4':
                    //if (len == 2)
                    //{
                    //    strNumber += " tư";
                    //}
                    //else
                    //{
                    strNumber += " bốn";
                    //}
                    break;

                case '5':
                    if (len == 2)
                        strNumber += " lăm";
                    else
                        strNumber += " năm";
                    break;

                case '6':
                    strNumber += " sáu";
                    break;

                case '7':
                    strNumber += " bảy";
                    break;

                case '8':
                    strNumber += " tám";
                    break;

                case '9':
                    strNumber += " chín";
                    break;
            }

            if (len == 3)
            {
                if (arr[1] != '0' && arr[1] != '1') strNumber += " mươi";
                if (arr[1] == '0' && arr[2] != '0') strNumber += " linh";
            }
        }

        // Hàng đơn vị
        if (len == 3)
            switch (arr[2])
            {
                case '1':
                    if (arr[1] != '0' && arr[1] != '1')
                        strNumber += " mốt";
                    else
                        strNumber += " một";
                    break;

                case '2':
                    strNumber += " hai";
                    break;

                case '3':
                    strNumber += " ba";
                    break;

                case '4':
                    //if ((arr[1] != '0') && (arr[1] != '1'))
                    //{
                    //    strNumber += " tư";
                    //}
                    //else
                    //{
                    strNumber += " bốn";
                    //}
                    break;

                case '5':
                    if (arr[1] != '0')
                        strNumber += " lăm";
                    else
                        strNumber += " năm";
                    break;

                case '6':
                    strNumber += " sáu";
                    break;

                case '7':
                    strNumber += " bảy";
                    break;

                case '8':
                    strNumber += " tám";
                    break;

                case '9':
                    strNumber += " chín";
                    break;
            }

        return strNumber;
    }

    public static string DecimalToString_AfterCommas(string pStrNumeric)
    {
        if (string.IsNullOrWhiteSpace(pStrNumeric))
            return "";
        var numberOfText = int.Parse(pStrNumeric);
        if (numberOfText == 0) return "";

        const string joinString = " phẩy";
        var strReadNumber = "";
        //loại bo so 0 ở cuối
        while (pStrNumeric.EndsWith("0"))
            pStrNumeric = pStrNumeric.Substring(0, pStrNumeric.Length - 1);
        if (pStrNumeric.StartsWith("0000") || (pStrNumeric.StartsWith("00") && pStrNumeric.Length >= 5))
        {
            var arr = pStrNumeric.ToCharArray();
            foreach (var t in arr)
                switch (t)
                {
                    case '0':
                        strReadNumber += " không";
                        break;

                    case '1':
                        strReadNumber += " một";
                        break;

                    case '2':
                        strReadNumber += " hai";
                        break;

                    case '3':
                        strReadNumber += " ba";
                        break;

                    case '4':
                        strReadNumber += " bốn";
                        break;

                    case '5':
                        strReadNumber += " năm";
                        break;

                    case '6':
                        strReadNumber += " sáu";
                        break;

                    case '7':
                        strReadNumber += " bảy";
                        break;

                    case '8':
                        strReadNumber += " tám";
                        break;

                    case '9':
                        strReadNumber += " chín";
                        break;
                }
        }
        else
        {
            var strNumber = "";
            var strRead3 = "";
            var i = 0;
            var len = 0;
            var len3 = 0;
            var mod3 = 0;
            // Loại bỏ dấu phân cách nhóm
            strNumber = pStrNumeric.Replace(",", "");
            strNumber = strNumber.Replace(".", "");

            // Doc chuỗi so
            len = strNumber.Length;
            // Dem bo so 3
            len3 = len / 3;
            mod3 = len % 3;

            // Neu doi dai chuỗi la 0 thi gan la so 0
            if (len == 0)
            {
                strNumber = "0";
                mod3 = 1;
            }

            // Doc bo so dau tien
            strRead3 = Len3ToStringDecimal(strNumber.Substring(0, mod3));
            strReadNumber = strRead3;
            for (i = 0; i < len3; i++)
            {
                if (3 * (len3 - i) % 9 == 0 && strReadNumber.Length > 0) strReadNumber += " tỷ";
                if (strRead3.Length > 0)
                {
                    if (3 * (len3 - i) % 9 == 6) strReadNumber += " triệu";
                    if (3 * (len3 - i) % 9 == 3) strReadNumber += " nghìn";
                }

                strRead3 = Len3ToStringDecimal(strNumber.Substring(mod3 + i * 3, 3));
                strReadNumber += strRead3;
            }
        }

        return joinString + strReadNumber;
    }

    public static string Len3ToStringDecimal(string pStrInt)
    {
        var strNumber = "";
        var len = pStrInt.Length;

        if (string.IsNullOrWhiteSpace(pStrInt)) return strNumber;

        if (pStrInt == "000") return " không";

        var array = pStrInt.ToCharArray();
        switch (array[0])
        {
            case '0':
                strNumber += " không";
                break;

            case '1':
                if (len == 2)
                    strNumber += " mười";
                else
                    strNumber += " một";
                break;

            case '2':
                strNumber += " hai";
                break;

            case '3':
                strNumber += " ba";
                break;

            case '4':
                strNumber += " bốn";
                break;

            case '5':
                strNumber += " năm";
                break;

            case '6':
                strNumber += " sáu";
                break;

            case '7':
                strNumber += " bảy";
                break;

            case '8':
                strNumber += " tám";
                break;

            case '9':
                strNumber += " chín";
                break;
        }

        if (len == 3)
            // hàng Trăm
            strNumber += " trăm";
        if (len == 2)
            if (array[0] != '0' && array[0] != '1')
                strNumber += " mươi";
        // Hàng Chục
        if (len >= 2)
        {
            switch (array[1])
            {
                case '1':
                    if (len == 3) strNumber += " mười";
                    if (len == 2 && array[0] != '1' && array[0] != '0') strNumber += " mốt";
                    if (len == 2 && (array[0] == '1' || array[0] == '0')) strNumber += " một";
                    break;

                case '2':
                    strNumber += " hai";
                    break;

                case '3':
                    strNumber += " ba";
                    break;

                case '4':
                    //if (len == 2)
                    //{
                    //    strNumber += " tư";
                    //}
                    //else
                    //{
                    strNumber += " bốn";
                    //}
                    break;

                case '5':
                    if (len == 2 && array[0] != '0')
                        strNumber += " lăm";
                    else
                        strNumber += " năm";
                    break;

                case '6':
                    strNumber += " sáu";
                    break;

                case '7':
                    strNumber += " bảy";
                    break;

                case '8':
                    strNumber += " tám";
                    break;

                case '9':
                    strNumber += " chín";
                    break;
            }

            if (len == 3)
            {
                if (array[1] != '0' && array[1] != '1') strNumber += " mươi";
                if (array[1] == '0' && array[2] != '0') strNumber += " linh";
            }
        }

        // Hàng đơn vị
        if (len == 3)
            switch (array[2])
            {
                case '1':
                    if (array[1] != '0' && array[1] != '1')
                        strNumber += " mốt";
                    else
                        strNumber += " một";
                    break;

                case '2':
                    strNumber += " hai";
                    break;

                case '3':
                    strNumber += " ba";
                    break;

                case '4':
                    //if ((arr[1] != '0') && (arr[1] != '1'))
                    //{
                    //    strNumber += " tư";
                    //}
                    //else
                    //{
                    strNumber += " bốn";
                    //}
                    break;

                case '5':
                    if (array[1] != '0')
                        strNumber += " lăm";
                    else
                        strNumber += " năm";
                    break;

                case '6':
                    strNumber += " sáu";
                    break;

                case '7':
                    strNumber += " bảy";
                    break;

                case '8':
                    strNumber += " tám";
                    break;

                case '9':
                    strNumber += " chín";
                    break;
            }

        return strNumber;
    }
    


    #endregion Bind data to Content
}