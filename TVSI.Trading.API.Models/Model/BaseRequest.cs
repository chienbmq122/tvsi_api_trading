using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.Trading.API.Models.Model;

public class BaseRequest
{
    [Required]
    [Description("Tài khoản đăng nhập")]
    public string UserName { get; set; }
}