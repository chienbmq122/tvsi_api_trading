﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Trading.API.Models.Model.Response.Common
{
    public class StockInfo
    {
        public int No { get; set; }
        public string Symbol { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class StockReponse
    {
        public int TotalItem { get; set; }
        public List<StockInfo> Items { get; set; }
    }

    public class StockSql
    {
        public int TotalItem { get; set; }
        public string Items { get; set; }
    }
}
