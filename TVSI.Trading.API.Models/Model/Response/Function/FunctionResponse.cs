﻿namespace TVSI.Trading.API.Models.Model.Response.Function;

public class FunctionResponse
{
    public int? fAllRight { get; set; }
    public int? fSubBranch { get; set; }
    public int? fOnlyBranch { get; set; }
    public int? fSubLevel { get; set; }
    public string? fBranchCode { get; set; }
    public string? fSaleIDs { get; set; }
    public string? UserName { get; set; }
    public string? BranchID { get; set; }
    public string? SaleID { get; set; }
}