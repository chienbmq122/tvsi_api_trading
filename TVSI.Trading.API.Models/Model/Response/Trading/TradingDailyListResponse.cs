﻿using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace TVSI.Trading.API.Models.Model.Response;

public class TradingDailyListResponse
{
    public decimal? TotalVolume { get; set; }
    public decimal? TotalValue { get; set; }
    public string? SaleID { get; set; }
    public string? AccountNo { get; set; }
    public string? CustCode { get; set; }

    public string? FullName
    {
        get
        {
            if (!string.IsNullOrEmpty(FullNameTxt))
            {
               return Regex.Replace(FullNameTxt, @"\(.*?\)", "").Trim();
            }

            return FullNameTxt;
        }
    }

    [JsonIgnore]
    public string? FullNameTxt { get; set; }
    
}