﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Text.Json.Serialization;

namespace TVSI.Trading.API.Models.Model.Response;

public class TradingDailyDetailResponse
{
    public IEnumerable<TradingDailyOrderVolumeResponse> TradingDailyVolume { get; set; } =
        new List<TradingDailyOrderVolumeResponse>();

    public IEnumerable<TradingDailyOrderResponse> TradingDailyOrder { get; set; } = 
        new List<TradingDailyOrderResponse>();
}

public class TradingDailyOrderVolumeResponse
{
    /// <summary>
    /// Mã Chứng khoán
    /// </summary>
    public string? StockCode { get; set; }
    
    /// <summary>
    /// Mã Khách hàng
    /// </summary>
    public string? AccountNo { get; set; }
    
    /// <summary>
    /// khối lượng
    /// </summary>
    public decimal Volume { get; set; }
    
    /// <summary>
    /// Mua và bán
    /// </summary>
    public string? Side { get; set; }
    
    
    /// <summary>
    /// giá TB Khớp
    /// </summary>
    public decimal AvgPrice { get; set; }
}

public class TradingDailyOrderResponse
{
    /// <summary>
    /// Mã Chứng khoán
    /// </summary>
    public string? StockCode { get; set; }
    
    
    /// <summary>
    /// khối lượng
    /// </summary>
    public decimal? Volume { get; set; }
    
    /// <summary>
    /// Số hiệu lệnh
    /// </summary>
    public decimal? OrderNo { get; set; }
    
    /// <summary>
    /// KL Khớp 
    /// </summary>
    public decimal? MatchVolume { get; set; }
    
    /// <summary>
    /// KL Hủy 
    /// </summary>
    public decimal? CancelVolume { get; set; }  
    
    /// <summary>
    /// Mua và bán
    /// </summary>
    public string? Side { get; set; } 
    
    /// <summary>
    /// giờ đặt lệnh
    /// </summary>
    public string? OrderTime {
        get
        {
            if (OrderTimeConvert != null)
            {
                return OrderTimeConvert?.ToString("HH:mm:ss");
            }
            return "";
        }
        
    }
    [JsonIgnore]
    public string? OrderTimeFormat { get; set; }
    [JsonIgnore]
    public DateTime? OrderTimeConvert
    {
        get
        {
            if (!string.IsNullOrEmpty(OrderTimeFormat))
            {
                return DateTime.ParseExact(OrderTimeFormat, "HHmmss",
                    System.Globalization.CultureInfo.InvariantCulture);
            }

            return null;
        }
    }
    
    
    
    /// <summary>
    /// giá đặt lệnh
    /// </summary>
    public decimal? OrderPrice { get; set; }
    
    /// <summary>
    /// loại lệnh
    /// </summary>
    public string? OrderDescription { get; set; }
    
    /// <summary>
    /// giá trung bình
    /// </summary>
    public decimal? AvgPrice { get; set; }
    


    public int? OrderStatus { get; set; }


    /// <summary>
    /// show giá lệnh
    /// </summary>
    public string? ShowPrice {
        get
        {
            decimal number;
            if (!string.IsNullOrEmpty(ShowPriceTxt))
            {
                if (Decimal.TryParse(ShowPriceTxt, out number))
                {
                    return decimal.Parse(ShowPriceTxt).ToString("F");
                }
                return ShowPriceTxt;
            }
            return "";
        }
        
    }
    
    [JsonIgnore]
    public string? ShowPriceTxt { get; set; }

}