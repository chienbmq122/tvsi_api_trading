﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Trading.API.Models.Model.Response.Trading
{
    public class TotalTradingReponse
    {
        public TotalTradingDaily DailyData { get; set; }
        public TotalTradingMonthly MonthlyData { get; set; }
    }

    public class TotalTradingDaily
    {
        public double TotalVolume { get; set; }

        public double TotalValue { get; set; }

        public double TotalComm { get; set; }
        public double TotalVat { get; set; }
        public double TotalPercent { get; set; }
    }

    public class TotalTradingMonthly
    {
        public double TotalVolume { get; set; }

        public double TotalValue { get; set; }

        public double TotalFee { get; set; }
        public double TotalFeeSale { get; set; }
        public double TotalPercent { get; set; }
    }
}
