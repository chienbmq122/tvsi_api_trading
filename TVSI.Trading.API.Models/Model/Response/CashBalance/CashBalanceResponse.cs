﻿using System.Diagnostics;

namespace TVSI.Trading.API.Models.Model.Response;

public class CashBalanceResponse
{
    public CashBalanceWithDrawal? CashBalanceWithDrawal { get; set; } = new CashBalanceWithDrawal();
    
    public IEnumerable<StockBalance> StockBalance { get; set; } = new List<StockBalance>();

}

public class StockBalance
{
    /// <summary>
    /// Mã CK
    /// </summary>
    public string? StockCode { get; set; }
    
    
    /// <summary>
    /// Khối lượng
    /// </summary>
    public int Volume { get; set; }
    
    /// <summary>
    /// Gía mua tb text
    /// </summary>
    public double AVGPRICET { get; set; }

    /// <summary>
    /// Gía mua tb
    /// </summary>
    public string AvgPrice
    {
        get
        {
            return AVGPRICET.ToString("N2");
        }
        
    }
    
    
    /// <summary>
    /// Tên trạng thái
    /// </summary>
    public string? StockTypeName {
        get
        {
            if(!string.IsNullOrEmpty(StockType))
            {
                switch (StockType)
                {
                    case "2":
                        return "CK Tự do chuyển nhượng";
                    case "3":
                        return "CK Lưu ký";
                    case "9":
                        return "CK Hưởng quyền";
                    case "12":
                        return "CK Hưởng quyền chờ GD"; 
                    case "42":
                        return "CK Mua chờ nhận về"; 
                    case "43":
                        return "CK Bán chờ chuyển đi";
                    case "46":
                        return "CK Cầm cố"; 
                    case "49":
                        return "CK Hạn chế chuyển nhượng"; 
                    case "30":
                        return "Cổ phiếu thưởng";
                    case "31":
                        return "Cổ phiếu thưởng HCCN";
                    default:
                        return "";
                }
            }
            return "";
        }
        
    } 
    
    /// <summary>
    ///  trạng thái int
    /// </summary>
    public string? StockType { get; set; } 
    
    /// <summary>
    /// Màu trạng thái
    /// </summary>
    public string? StockTypeColor { get; set; }

    /// <summary>
    /// Tài khoản KH
    /// </summary>
    public string? AccountNo { get; set; }


}

public class CashBalanceWithDrawal
{
    /// <summary>
    /// tài khoản đuổi 1
    /// </summary>
    public string? AccountNo { get; set; }
    /// <summary>
    /// Tài khoản đuôi 1
    /// </summary>
    public double MaxWithdrawalAccount { get; set; }
    
    
    /// <summary>
    /// Tài khoản đuôi 6
    /// </summary>
    public string? AccountNoMargin { get; set; }
    /// <summary>
    /// Tài khoản đuôi 6
    /// </summary>
    public double MaxWithdrawalAccountMargin { get; set; }
}
public class APIReponse
{
    public string? RetCode { get; set; }
    public RetData RetData { get; set; }
}

public class BankInfo
{
    public int CustBankId { get; set; }
    public string? BankAccountNo { get; set; }
    public string? BankAccountName { get; set; }
    public string? BankName { get; set; }
}

public class BlockFeeInfo
{
    public string? FeeType { get; set; }
    public string? FeeName { get; set; }
    public double Amount { get; set; }
}

public class CashBalanceInfo
{
    public string? AccountNo { get; set; }
    public double Balance { get; set; }
    public double Withdrawal { get; set; }
    public double BlockFee { get; set; }
    public double MinMarginRate { get; set; }
    public double MaxWithdrawal { get; set; }
    public double FastWithdrawal { get; set; }
}

public class InternalAccountInfo
{
    public string? CustCode { get; set; }
    public string? InternalAccountNo { get; set; }
    public string? InternalAccountName { get; set; }
}

public class RetData
{
    public string? AccountNo { get; set; }
    public List<BankInfo> BankInfos { get; set; }
    public CashBalanceInfo CashBalanceInfo { get; set; }
    public List<InternalAccountInfo> InternalAccountInfos { get; set; }
    public List<BlockFeeInfo> BlockFeeInfos { get; set; }
}