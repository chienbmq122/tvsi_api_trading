﻿namespace TVSI.Trading.API.Models.Model.Request.CashBalance;

public class CashBalanceRequest : BaseRequest
{
    
    /// <summary>
    /// Mã KH
    /// </summary>
    public string? CustCode { get; set; }
    
    
    /*/// <summary>
    /// Thứ tự trang (tính từ 1)
    /// </summary>
    public int PageIndex { get; set; }

    /// <summary>
    /// Số bản ghi cần lấy trên một trang
    /// </summary>
    public int PageSize { get; set; }*/
}