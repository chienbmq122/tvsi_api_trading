﻿namespace TVSI.Trading.API.Models.Model.Request.Common;

public class GetAllRightByFunctionRequest : BaseRequest
{
    public string? Function { get; set; }

}