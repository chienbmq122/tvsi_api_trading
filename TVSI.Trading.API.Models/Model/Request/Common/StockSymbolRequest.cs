﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.Trading.API.Models.Model.Request.Common
{
    public class StockSymbolRequest : BaseRequest
    {
        public string Symbol { get; set; }
    }
}
