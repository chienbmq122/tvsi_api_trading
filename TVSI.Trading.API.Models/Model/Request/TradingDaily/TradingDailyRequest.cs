namespace TVSI.Trading.API.Models.Model.Request.TradingDaily;

public class TradingDailyRequest : BaseRequest
{
    /// <summary>
    /// Số tk khách hàng
    /// </summary>
    public string? CustCode { get; set; }
    
    public int PageSize{ get;set;}
    public int PageIndex{ get;set;}
}