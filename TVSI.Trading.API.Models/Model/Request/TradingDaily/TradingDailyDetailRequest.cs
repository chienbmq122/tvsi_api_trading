﻿namespace TVSI.Trading.API.Models.Model.Request.TradingDaily;

public class TradingDailyDetailRequest : BaseRequest
{
    public string? CustCode { get; set; }
    public string? StockCode { get; set; }
    public string? Side { get; set; }
    public int? OrderStatus { get; set; }
}