﻿using TVSI.Trading.API.Models.Model;
using TVSI.Trading.API.Models.Model.Request.CashBalance;
using TVSI.Trading.API.Models.Model.Response;

namespace TVSI.Trading.API.Service.Interfaces;


/// <summary>
/// InterFace
/// </summary>
public interface ICashBalanceService
{
    /// <summary>
    /// Lấy số dư tiền có thể rút
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    Task<ResponseSingle<CashBalanceResponse>> GetBalanceWithdrawalCustomer(CashBalanceRequest model);
}