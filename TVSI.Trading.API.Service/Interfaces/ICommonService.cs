﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.Trading.API.Models.Model;
using TVSI.Trading.API.Models.Model.Request.Common;
using TVSI.Trading.API.Models.Model.Response.Common;

namespace TVSI.Trading.API.Service.Interfaces
{
    public interface ICommonService
    {
        Task<ResponseSingle<dynamic>> GetStockSymbol(StockSymbolRequest model);
        /// <summary>
        /// Lấy thông tin mã CK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns> 


        /// <summary>
        /// Lấy danh sách loại lệnh
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Response<dynamic>> GetOrderStatus(BaseRequest model);


        /// <summary>
        /// Lấy quyền full
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userName"></param>
        /// <param name="function"></param>
        /// <returns></returns>
        Task<ResponseSingle<dynamic>> GetAllRightByFun(string userName,string function);


    }
}
