using TVSI.Trading.API.Models.Model;
using TVSI.Trading.API.Models.Model.Request.TradingDaily;
using TVSI.Trading.API.Models.Model.Response;
using TVSI.Trading.API.Models.Model.Response.Trading;

namespace TVSI.Trading.API.Service.Interfaces;

public interface ITradingDailyService
{
    Task<Response<dynamic>> GetTradingDailyList(TradingDailyRequest model);
    Task<ResponseSingle<TotalTradingReponse>> GetTradingTotal(BaseRequest model);
    Task<ResponseSingle<TradingDailyDetailResponse>> GetTradingDailyDetailInfo(TradingDailyDetailRequest model);
    
}