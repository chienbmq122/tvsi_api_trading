using Helper.DataAccess.Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace TVSI.Trading.API.Service;

public class BaseService<T> where T : class
{
    protected readonly IConfiguration _config;
    protected readonly ILogger<T> _logger;


    /// <summary>
    /// 
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="config"></param>
    protected BaseService(ILogger<T> logger, IConfiguration config)
    {
        _logger = logger;
        _config = config;
    }
}