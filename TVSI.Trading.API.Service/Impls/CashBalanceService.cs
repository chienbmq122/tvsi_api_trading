﻿using System.Reflection;
using System.Text;
using Dapper;
using Helper.DataAccess.Dapper;
using Helper.Security;
using IBM.Data.DB2.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TVSI.Trading.API.Models.Model;
using TVSI.Trading.API.Models.Model.Request.CashBalance;
using TVSI.Trading.API.Models.Model.Response;
using TVSI.Trading.API.Models.Model.Response.Trading;
using TVSI.Trading.API.Service.Interfaces;
using TVSI.XTRADE.BO.API.Common;
using TVSI.XTRADE.BO.API.Models.Enums;

namespace TVSI.Trading.API.Service.Impls;

/// <summary>
/// Số dư tài khoản
/// </summary>
public class CashBalanceService : BaseService<CashBalanceService>, ICashBalanceService
{
    private readonly IDapperHelper? _dapper;
    private readonly string? _innoTradeConn;
    private readonly string? _fisTradeConn;
    private readonly string? _crmDbConn;
    private readonly string? _commonDbConn;
    private readonly string? _apiBond;
    private readonly int _sqlTimeout;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="config"></param>
    public CashBalanceService(ILogger<CashBalanceService> logger, IConfiguration config) : base(logger, config)
    {
        var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        var connStr = _config.GetConnectionString("InnoTrade");
        var commonStr = _config.GetConnectionString("COMMONDB_CONNECTION");
        var fisStr = _config.GetConnectionString("FISDB_CONNECTION");
        var apiBond = _config["TvsiApi:BOND_API"];
        var crmStr = _config.GetConnectionString("CRMDB_CONNECTION");
        _innoTradeConn = /*env != "Development" ? Crypto.Decrypt(connStr!, CommonConstants.EncryptionKeys) :*/ connStr;
        _crmDbConn = /*env != "Development" ? Crypto.Decrypt(crmStr, CommonConstants.EncryptionKeys) :*/ crmStr;
        _fisTradeConn = /*env != "Development" ? Crypto.Decrypt(fisStr!, CommonConstants.EncryptionKeys) :*/ fisStr;
        _fisTradeConn = fisStr;
        _dapper = _dapper;
        _apiBond = apiBond;
        _commonDbConn = commonStr;
        _innoTradeConn = connStr;
        _sqlTimeout = _config["Timeout:Database"] == null
            ? CommonConstants.SqlServerTimeout
            : int.Parse(_config["Timeout:Database"]!);
    }

    /// <summary>
    /// Lấy số dư tiền
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    public async Task<ResponseSingle<CashBalanceResponse>> GetBalanceWithdrawalCustomer(CashBalanceRequest model)
    {
        try
        {
            var data = new CashBalanceResponse();
            using (var client = new HttpClient())
            {
                var param = new
                {
                    UserId = model.CustCode,
                    AccountNo = model.CustCode + "1"
                };
                var json = JsonConvert.SerializeObject(param);
                var body = new StringContent(json, Encoding.UTF8, "application/json");
                using (var response = await client.PostAsync(_apiBond + "CashTransfer/CT_CI_CustCashTransferInfo", body))
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var dataAccount = JsonConvert.DeserializeObject<APIReponse>(result);
                    if (dataAccount != null)
                    {
                        data.CashBalanceWithDrawal!.AccountNo = model.CustCode + "1";
                        if (dataAccount.RetCode == "000")
                            data.CashBalanceWithDrawal!.MaxWithdrawalAccount = 
                                dataAccount.RetData.CashBalanceInfo.MaxWithdrawal < 0 ? 0 : dataAccount.RetData.CashBalanceInfo.MaxWithdrawal;
                    }
                }

                var paramMargin = new
                {
                    UserId = model.CustCode,
                    AccountNo = model.CustCode + "6"
                };
                var jsonMargin = JsonConvert.SerializeObject(paramMargin);
                var bodyMargin = new StringContent(jsonMargin, Encoding.UTF8, "application/json");
                using (var response =
                       await client.PostAsync(_apiBond + "CashTransfer/CT_CI_CustCashTransferInfo", bodyMargin))
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var dataAccountMargin = JsonConvert.DeserializeObject<APIReponse>(result);
                    if (dataAccountMargin != null)
                    {
                        data.CashBalanceWithDrawal!.AccountNoMargin = model.CustCode + "6";
                        if (dataAccountMargin.RetCode == "000")
                            data.CashBalanceWithDrawal!.MaxWithdrawalAccountMargin =
                                dataAccountMargin.RetData.CashBalanceInfo.MaxWithdrawal < 0 ? 0 : dataAccountMargin.RetData.CashBalanceInfo.MaxWithdrawal;
                    }
                }

                var sql = @"SELECT TRIM(ACCOUNTNO) ACCOUNTNO, TRIM(SECSYMBOL) STOCKCODE,
                                SECTYPE StockType,
                            AVAIVOLUME Volume, AVGPRICE AVGPRICET
                            FROM CUSTPOSITIONTAB WHERE ACCOUNTNO = @AccountNo AND SECTYPE IN (2,9,10,12,49,46,30,42)
                            UNION ALL 
                            SELECT TRIM(ACCOUNTNO) ACCOUNTNO, TRIM(STOCK_SYM) STOCKCODE, STOCK_TYPE StockType, ACTUAL_VOL Volume, AVG_COST AVGPRICET
                            FROM CUSTPOSITIONTAB_CB
                            WHERE ACCOUNTNO = @AccountNoMargin AND STOCK_TYPE IN (2,9,10,12,49,46,30,42)";

                using (var conn = new DB2Connection(_fisTradeConn))
                {
                    conn.Open();
                    var dataList = await conn.QueryAsync<StockBalance>(sql, new
                    {
                        AccountNo = model.CustCode + "1",
                        AccountNoMargin = model.CustCode + "6"
                    });
                    if (dataList != null && dataList.Any())
                        data.StockBalance = dataList;
                }
                return new ResponseSingle<CashBalanceResponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = data
                };
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<CashBalanceResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }
}