using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using Dapper;
using Helper.DataAccess.Dapper;
using IBM.Data.DB2.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using TVSI.Trading.API.Models.Model;
using TVSI.Trading.API.Models.Model.Request.TradingDaily;
using TVSI.Trading.API.Models.Model.Response;
using TVSI.Trading.API.Models.Model.Response.Function;
using TVSI.Trading.API.Models.Model.Response.Trading;
using TVSI.Trading.API.Service.Interfaces;
using TVSI.XTRADE.BO.API.Common;
using TVSI.XTRADE.BO.API.Models.Enums;


namespace TVSI.Trading.API.Service.Impls;

/// <summary>
/// 
/// </summary>
public class TradingDailyService : BaseService<TradingDailyService>,ITradingDailyService
{
    private readonly IDapperHelper _dapper;
    private readonly string? _innoTradeConn;
    private readonly string? _fisTradeConn;
    private readonly string? _crmDbConn;
    private readonly string? _emsDbConn;

    private readonly int _sqlTimeout;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="logger"></param>
    /// <param name="config"></param>
    /// <param name="dapper"></param>
    public TradingDailyService(ILogger<TradingDailyService> logger, IConfiguration config, IDapperHelper dapper) :
        base(logger, config)
    {
        var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        var connStr = _config.GetConnectionString("InnoTrade");
        var fisStr = _config.GetConnectionString("FISDB_CONNECTION");
        var crmStr = _config.GetConnectionString("CRMDB_CONNECTION");
        var emsStr = _config.GetConnectionString("EMSDB_CONNECTION");
        _innoTradeConn = connStr;
        _crmDbConn = crmStr;
        _fisTradeConn = fisStr;
        _emsDbConn =  emsStr;
        _fisTradeConn = fisStr;
        _dapper = dapper;
        _innoTradeConn = connStr;
        _sqlTimeout = _config["Timeout:Database"] == null
            ? CommonConstants.SqlServerTimeout
            : int.Parse(_config["Timeout:Database"]!);
    }

    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public async Task<Response<dynamic>> GetTradingDailyList(TradingDailyRequest model)
    {
        try
        {
            var start = (model.PageIndex - 1) * model.PageSize;
            var sql = "SELECT Sum(A.MATCHVOLUME) TotalVolume, Sum(A.MATCHVALUE * 1000) TotalValue,TRIM(A.TRADERID) SaleID, TRIM(A.ACCOUNTNO) ACCOUNTNO,SUBSTR(A.ACCOUNTNO,1,6) CustCode, TRIM(B.CUSTNAME) FullNameTxt FROM ORDERTAB A INNER JOIN CUSTTAB B ON A.ACCOUNTNO = B.ACCOUNTNO WHERE A.TRADERID = @UserName {0} GROUP BY B.CustName,A.ACCOUNTNO, A.TRADERID,SUBSTR(A.ACCOUNTNO,1,6)  ORDER BY  SUM(A.MATCHVOLUME),Sum(A.MATCHVALUE * 1000) DESC";
            sql = sql.Replace("{0}", !string.IsNullOrEmpty(model.CustCode) ? "AND A.ACCOUNTNO LIKE @CustCode" : "");
            using (var conn = new DB2Connection(_fisTradeConn))
            {
                conn.Open();
                var data = await conn.QueryAsync<TradingDailyListResponse>(sql, new
                {
                    @UserName = model.UserName.Substring(0,4),
                    @CustCode = "%" + model.CustCode + "%",
                    @Start = start,
                    @PageSize = model.PageSize
                });

                return new Response<dynamic>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = data
                };
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<dynamic>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }
    
    public async Task<ResponseSingle<TradingDailyDetailResponse>> GetTradingDailyDetailInfo(TradingDailyDetailRequest model)
    {
        try
        {
            var data = new TradingDailyDetailResponse();
            data.TradingDailyOrder = new List<TradingDailyOrderResponse>();
            
            var sql = "SELECT TRIM(A.SECSYMBOL) StockCode, A.ORDERNO," +
                      " A.VOLUME VOLUME, A.MATCHVOLUME MATCHVOLUME,A.CANCELVOLUME CANCELVOLUME,A.SIDE SIDE,A.ORDERTIME OrderTimeFormat,A.PRICE ORDERPRICE,TRIM(A.ShowPrice) ShowPriceTxt," +
                      "CASE " +
                      "WHEN ORDERSTATUS='M' OR MATCHVOLUME=VOLUME THEN 'Lệnh đã khớp hoàn toàn' " +
                      "WHEN MATCHVOLUME<VOLUME and MATCHVOLUME>0 THEN 'Lệnh đã khớp 1 phần'" +
                      "WHEN MATCHVOLUME<VOLUME and MATCHVOLUME>0 and orderstatus in ('X','XA','XO') THEN 'Lệnh đã khớp 1 phần và hủy phần còn lại'" +
                      "WHEN orderstatus='PO' THEN 'Lệnh chờ tại TVSI'" +
                      "WHEN orderstatus='O' THEN 'Lệnh chờ khớp tại sàn'" +
                      "WHEN orderstatus in ('X') THEN 'Lệnh đã được hủy'" +
                      "WHEN orderstatus in ('R') THEN 'Lệnh bị từ chối bởi TVSI'" +
                      "WHEN orderstatus in ('R') and (MKTORDNO!='' or MKTORDNO is not null) THEN 'Lệnh bị từ chối bởi sàn'ELSE '' END ORDERDESCRIPTION," +
                      "CASE " +
                      "WHEN ORDERSTATUS='M' OR MATCHVOLUME=VOLUME THEN 1 " +
                      "WHEN MATCHVOLUME<VOLUME and MATCHVOLUME>0  THEN  2 " +
                      "WHEN MATCHVOLUME<VOLUME and MATCHVOLUME>0 and orderstatus in ('X','XA','XO') THEN 3 " +
                      "WHEN orderstatus='PO' THEN 4 " +
                      "WHEN orderstatus='O' THEN 5 " +
                      "WHEN orderstatus in ('X') THEN 6 " +
                      "WHEN orderstatus in ('R') THEN 7 " +
                      "WHEN orderstatus in ('R') and (MKTORDNO!='' or MKTORDNO is not null) THEN 8 ELSE 0 END ORDERSTATUS" +
                      ",COALESCE(AVGPRICE, 0.00) AVGPRICE FROM ORDERTAB A LEFT JOIN (SELECT ORDERNO, SUM(DEALVOLUME * DEALPRICE)/SUM(DEALVOLUME) AVGPRICE FROM DEALTAB WHERE " +
                      "ACCOUNTNO LIKE @AccountNo GROUP BY ORDERNO) AS B ON A.ORDERNO =  B.ORDERNO WHERE A.TRADERID = SUBSTR(@UserName,1,4) AND ACCOUNTNO LIKE @AccountNo {0}{1}{2}";
            
            sql = sql.Replace("{0}", !string.IsNullOrEmpty(model.StockCode) ? " AND SECSYMBOL LIKE @StockCode " : " ");
            sql = sql.Replace("{1}", !string.IsNullOrEmpty(model.Side) ? " AND SIDE = @Side " : " ");

            if (model.OrderStatus == 0)
                sql = sql.Replace("{2}", model.OrderStatus == 0 ? "" : " ");   
            if (model.OrderStatus == 1)
                sql = sql.Replace("{2}", model.OrderStatus == 1 ? " AND ( orderstatus = 'M' OR MATCHVOLUME=VOLUME ) " : " ");
            if (model.OrderStatus == 2)
                sql = sql.Replace("{2}", model.OrderStatus == 2 ? " AND ( A.MATCHVOLUME < A.VOLUME and A.MATCHVOLUME > 0 )" : " ");
            if (model.OrderStatus == 3)
                sql = sql.Replace("{2}", model.OrderStatus == 3 ? " AND ( A.MATCHVOLUME < A.VOLUME and A.MATCHVOLUME > 0 and orderstatus in ('X','XA','XO') )" : " "); 
            if (model.OrderStatus == 4)
                sql = sql.Replace("{2}", model.OrderStatus == 4 ? " AND ( orderstatus = 'PO' ) " : " ");  
            if (model.OrderStatus == 5)
                sql = sql.Replace("{2}", model.OrderStatus == 5 ? " AND ( orderstatus = 'O' ) " : " "); 
            if (model.OrderStatus == 6)
                sql = sql.Replace("{2}", model.OrderStatus == 6 ? " AND ( orderstatus IN ('X') )" : " ");
            if (model.OrderStatus == 7)
                sql = sql.Replace("{2}", model.OrderStatus == 7 ? " AND ( orderstatus IN ('R') )" : " "); 
            if (model.OrderStatus == 8)
                sql = sql.Replace("{2}", model.OrderStatus == 8 ? " AND ( orderstatus IN ('R') and (MKTORDNO!='' or MKTORDNO is not null) ) " : " ");
            
            using (var conn = new DB2Connection(_fisTradeConn))
            {
                conn.Open();
                var dataVolume = await conn.QueryAsync<TradingDailyOrderResponse>(sql, new
                {
                    @AccountNo = "%" +model.CustCode + "%",
                    @StockCode = "%" +  model.StockCode + "%",
                    @Side = model.Side,
                    @OrderStatus = model.OrderStatus,
                    @UserName = model.UserName.Substring(0,4)
                });
                if (dataVolume != null)
                    data.TradingDailyOrder = dataVolume;
                
                var sqlOrder = "SELECT TRIM(A.SECSYMBOL) STOCKCODE, TRIM(A.ACCOUNTNO) ACCOUNTNO, A.VOLUME, A.SIDE SIDE,COALESCE(AVGPRICE, 0.00) AVGPRICE FROM ORDERTAB" +
                               " A LEFT JOIN ( SELECT ORDERNO, SUM(DEALVOLUME * DEALPRICE)/SUM(DEALVOLUME) AVGPRICE FROM DEALTAB" +
                               " WHERE ACCOUNTNO LIKE @AccountNo GROUP BY ORDERNO ) AS B ON A.ORDERNO = B.ORDERNO WHERE A.TRADERID = SUBSTR(@UserName,1,4) AND A.ACCOUNTNO LIKE @AccountNo {0}{1}{2} AND" +
                               "((ORDERSTATUS='M' OR MATCHVOLUME=VOLUME) OR (MATCHVOLUME<VOLUME and MATCHVOLUME>0) OR (MATCHVOLUME<VOLUME and MATCHVOLUME>0 and orderstatus in ('X','XA','XO'))) GROUP BY A.SECSYMBOL,A.ACCOUNTNO,A.VOLUME,A.SIDE,AVGPRICE";
                sqlOrder = sqlOrder.Replace("{0}", !string.IsNullOrEmpty(model.Side) ? " AND SIDE = @Side " : " ");
                sqlOrder = sqlOrder.Replace("{1}", !string.IsNullOrEmpty(model.StockCode) ? " AND SECSYMBOL LIKE @StockCode " : " ");
                if (model.OrderStatus == 0)
                    sqlOrder = sqlOrder.Replace("{2}", model.OrderStatus == 0 ? "" : " ");   
                if (model.OrderStatus == 1)
                    sqlOrder = sqlOrder.Replace("{2}", model.OrderStatus == 1 ? " AND ( orderstatus = 'M' OR MATCHVOLUME=VOLUME )" : " ");
                if (model.OrderStatus == 2)
                    sqlOrder = sqlOrder.Replace("{2}", model.OrderStatus == 2 ? " AND ( A.MATCHVOLUME < A.VOLUME and A.MATCHVOLUME > 0 )" : " ");
                if (model.OrderStatus == 3)
                    sqlOrder = sqlOrder.Replace("{2}", model.OrderStatus == 3 ? " AND ( A.MATCHVOLUME < A.VOLUME and A.MATCHVOLUME > 0 and orderstatus in ('X','XA','XO') ) " : " "); 
                if (model.OrderStatus == 4)
                    sqlOrder = sqlOrder.Replace("{2}", model.OrderStatus == 4 ? " AND ( orderstatus = 'PO' ) " : " ");  
                if (model.OrderStatus == 5)
                    sqlOrder = sqlOrder.Replace("{2}", model.OrderStatus == 5 ? " AND ( orderstatus = 'O' ) " : " "); 
                if (model.OrderStatus == 6)
                    sqlOrder = sqlOrder.Replace("{2}", model.OrderStatus == 6 ? " AND ( orderstatus IN ('X') ) " : " ");
                if (model.OrderStatus == 7)
                    sqlOrder = sqlOrder.Replace("{2}", model.OrderStatus == 7 ? " AND ( orderstatus IN ('R') )" : " "); 
                if (model.OrderStatus == 8)
                    sqlOrder = sqlOrder.Replace("{2}", model.OrderStatus == 8 ? " AND ( orderstatus IN ('R') and (MKTORDNO!='' or MKTORDNO is not null) ) " : " ");
                
                var dataOrder = await conn.QueryAsync<TradingDailyOrderVolumeResponse>(sqlOrder, new
                {
                    @AccountNo = "%" +model.CustCode+ "%",
                    @UserName = model.UserName.Substring(0,4),
                    @Side = model.Side,
                    @StockCode = "%" +  model.StockCode + "%",
                    @OrderStatus = model.OrderStatus,
                });
                if (dataOrder != null)
                    data.TradingDailyVolume = dataOrder;
                
                return new ResponseSingle<TradingDailyDetailResponse>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = data
                };
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<TradingDailyDetailResponse>()
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }

    public async Task<ResponseSingle<TotalTradingReponse>> GetTradingTotal(BaseRequest model)
    {
        try
        {
            TotalTradingDaily dataDaily = new TotalTradingDaily();
            TotalTradingDaily dataLastDaily = new TotalTradingDaily();
            TotalTradingMonthly dataMonthly = new TotalTradingMonthly();
            TotalTradingMonthly dataLastMonthly = new TotalTradingMonthly();

            var sql = @"SELECT SUM(MATCHVOLUME) as TotalVolume, SUM(MATCHVALUE * 1000) as TotalValue, SUM(SUMCOMM) as TotalComm,SUM(SUMVAT * 1000) as TotalVat FROM ORDERTAB LEFT JOIN COMMVATTAB ON ORDERTAB.ORDERNO = COMMVATTAB.ORDERNO WHERE TRADERID = @userName";
            using (var conn = new DB2Connection(_fisTradeConn))
            {
                conn.Open();
                var data = await conn.QueryAsync<TotalTradingDaily>(sql, new
                {
                    userName = model.UserName.Substring(0,4)
                });

                dataDaily = data.ToList()[0];
            }

            var sqlMonthly = @"SELECT SUm(khoi_luong_khop) as TotalVolume
		                            ,SUm(gia_tri_tien) as TotalValue
                                  ,Sum(phi_giao_dich) as TotalFee
                                  ,Sum(phi_giao_dich_thuc_nhan) as TotalFeeSale
                              FROM TVSI_DU_LIEU_PHI_VA_THUE WHERE MONTH(ngay_giao_dich) = @month AND YEAR(ngay_giao_dich) = @year AND ma_nhan_vien_quan_ly = @userName";
            using (var conn = new SqlConnection(_emsDbConn))
            {
                conn.Open();
                var data = await conn.QueryAsync<TotalTradingMonthly>(sqlMonthly, new
                {
                    userName = model.UserName,
                    month = DateTime.Now.Month,
                    year = DateTime.Now.Year,
                });
                dataMonthly = data.ToList()[0];
            }

            var sqlLastDay = @"SELECT SUm(khoi_luong_khop) as TotalVolume
		                            ,SUm(gia_tri_tien) as TotalValue
                                  ,Sum(phi_giao_dich) as TotalFee
                                  ,Sum(phi_giao_dich_thuc_nhan) as TotalFeeSale
                              FROM TVSI_DU_LIEU_PHI_VA_THUE WHERE DAY(ngay_giao_dich) = @day AND MONTH(ngay_giao_dich) = @month AND YEAR(ngay_giao_dich) = @year AND ma_nhan_vien_quan_ly = @userName";
            using (var conn = new SqlConnection(_emsDbConn))
            {
                conn.Open();
                var data = await conn.QueryAsync<TotalTradingDaily>(sqlLastDay, new
                {
                    userName = model.UserName,
                    day = DateTime.Now.Day - 1,
                    month = DateTime.Now.Month,
                    year = DateTime.Now.Year,
                });
                dataLastDaily = data.ToList()[0];
            }

            var sqlLastMonthly = @"SELECT SUm(khoi_luong_khop) as TotalVolume
		                            ,SUm(gia_tri_tien) as TotalValue
                                  ,Sum(phi_giao_dich) as TotalFee
                                  ,Sum(phi_giao_dich_thuc_nhan) as TotalFeeSale
                              FROM TVSI_DU_LIEU_PHI_VA_THUE WHERE MONTH(ngay_giao_dich) = @month AND YEAR(ngay_giao_dich) = @year AND ma_nhan_vien_quan_ly = @userName";
            using (var conn = new SqlConnection(_emsDbConn))
            {
                conn.Open();
                var data = await conn.QueryAsync<TotalTradingMonthly>(sqlLastMonthly, new
                {
                    userName = model.UserName,
                    month = DateTime.Now.Month - 1,
                    year = DateTime.Now.Year,
                });
                dataLastMonthly = data.ToList()[0];
            }

            TotalTradingReponse dataResponse = new TotalTradingReponse();

            dataDaily.TotalPercent = dataLastDaily.TotalValue == 0.0 ? 100 : Math.Round(((dataDaily.TotalValue / dataLastDaily.TotalValue) * 100), 2);
            dataMonthly.TotalPercent = dataMonthly.TotalValue == 0.0 ? 100 : Math.Round(((dataMonthly.TotalValue / dataLastMonthly.TotalValue) * 100), 2);

            dataResponse.DailyData = dataDaily;
            dataResponse.MonthlyData = dataMonthly;

            return new ResponseSingle<TotalTradingReponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = dataResponse
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<TotalTradingReponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }
    
    
    /// <summary>
    /// lấy quyền
    /// </summary>
    /// <param name="model"></param>
    /// <param name="userName"></param>
    /// <param name="function"></param>
    /// <returns></returns>
    public async Task<ResponseSingle<FunctionResponse>> GetAllRightByFunction(string userName,string? function)
    {
        try
        {
            var param = new DynamicParameters();
            param.Add("@UserName", userName, DbType.String, ParameterDirection.Input);
            param.Add("@Function", function, DbType.String, ParameterDirection.Input); // quyền view màn hình CustInfo CRM
            return new ResponseSingle<FunctionResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Success.ToEnumDescription(),
                Data = await _dapper.GetAsync<FunctionResponse>(_crmDbConn,
                    "TVSI_sGET_GET_ALL_RIGHT_BY_FUNCTION", param, _sqlTimeout)
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new ResponseSingle<FunctionResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = ErrorCodeDetail.Exception.ToEnumDescription()
            };
        }
    }
}