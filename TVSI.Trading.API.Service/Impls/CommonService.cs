﻿using Dapper;
using Helper.DataAccess.Dapper;
using Helper.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using IBM.Data.DB2.Core;
using TVSI.Trading.API.Models.Model;
using TVSI.Trading.API.Models.Model.Request.Common;
using TVSI.Trading.API.Models.Model.Response.Common;
using TVSI.Trading.API.Service.Interfaces;
using TVSI.XTRADE.BO.API.Common;
using TVSI.XTRADE.BO.API.Models.Enums;

namespace TVSI.Trading.API.Service.Impls
{
    /// <summary>
    /// Common Service
    /// </summary>
    public class CommonService : BaseService<CommonService>, ICommonService
    {
        private readonly IDapperHelper _dapper;
        private readonly string _priceStrConn;

        private readonly int _sqlTimeout;

        private readonly string? _innoTradeConn;
        private readonly string? _fisTradeConn;
        private readonly string? _crmDbConn;
        private readonly string? _commonDbConn;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="config"></param>
        /// <param name="dapper"></param>
        public CommonService(ILogger<CommonService> logger, IConfiguration config, IDapperHelper dapper) :
            base(logger, config)
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var connStr = _config.GetConnectionString("InnoTrade");
            var commonStr = _config.GetConnectionString("COMMONDB_CONNECTION");
            var fisStr = _config.GetConnectionString("FISDB_CONNECTION");
            var crmStr = _config.GetConnectionString("CRMDB_CONNECTION");
            var connStrPrice = _config.GetConnectionString("PRICEINOFO_CONNECTION");
            _priceStrConn = connStrPrice;
            _innoTradeConn = /*env != "Development" ? Crypto.Decrypt(connStr!, CommonConstants.EncryptionKeys) :*/ connStr;
            _crmDbConn = /*env != "Development" ? Crypto.Decrypt(crmStr, CommonConstants.EncryptionKeys) :*/ crmStr;
            _fisTradeConn = /*env != "Development" ? Crypto.Decrypt(fisStr!, CommonConstants.EncryptionKeys) :*/ fisStr;
            _fisTradeConn = fisStr;
            _dapper = dapper;
            _commonDbConn = commonStr;
            _innoTradeConn = connStr;
            _sqlTimeout = _config["Timeout:Database"] == null
                ? CommonConstants.SqlServerTimeout
                : int.Parse(_config["Timeout:Database"]!);
        }

        public async Task<ResponseSingle<dynamic>> GetStockSymbol(StockSymbolRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@Symbol", "", DbType.String, ParameterDirection.Input);
                var data = await _dapper.SelectAsync<StockInfo>(_priceStrConn, "BBB01_GET_LIST_INFO_STOCK", param, _sqlTimeout);

                return new ResponseSingle<dynamic>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = data
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<dynamic>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Exception.ToEnumDescription()
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual async Task<Response<dynamic>> GetOrderStatus(BaseRequest model)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@Type", 6, DbType.Int32, ParameterDirection.Input); // Value 6 là trạng thái order
                return new Response<dynamic>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = await _dapper.SelectAsync<dynamic>(_commonDbConn,
                        "TVSI_sGET_STATUS", param, _sqlTimeout)
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new Response<dynamic>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Exception.ToEnumDescription()
                };
            }
        }

        /// <summary>
        /// lấy quyền
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userName"></param>
        /// <param name="function"></param>
        /// <returns></returns>
        public async Task<ResponseSingle<dynamic>> GetAllRightByFun(string userName,string function)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@UserName", userName, DbType.String, ParameterDirection.Input);
                param.Add("@Function", function, DbType.String, ParameterDirection.Input); // quyền view màn hình CustInfo CRM
                return new ResponseSingle<dynamic>
                {
                    Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Success.ToEnumDescription(),
                    Data = await _dapper.SelectAsync<dynamic>(_crmDbConn,
                        "TVSI_sGET_GET_ALL_RIGHT_BY_FUNCTION", param, _sqlTimeout)
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new ResponseSingle<dynamic>
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = ErrorCodeDetail.Exception.ToEnumDescription()
                };
            }
        }
    }
}
